/*
        Copyright 2021 Ian Tester

        This file is part of Photo Finish.

        Photo Finish is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        Photo Finish is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with Photo Finish.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace CMS {

  template <typename T>
  T Format::scaleval(void) {
    if (is_8bit())
      return 255;

    if (is_16bit())
      return 65535;

    if (is_32bit())
      return 4294967295;

    return 1;	// Assume floating point
  }


}; // namespace CMS
