/*
	Copyright 2021 Ian Tester

	This file is part of Photo Finish.

	Photo Finish is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Photo Finish is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Photo Finish.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace PhotoFinish {

  template <typename T>
  definable<T>::definable() :
    _defined(false),
    _item()
  {}

  template <typename T>
  definable<T>::definable(const T &i) :
    _defined(true),
    _item(i)
  {}

  template <typename T>
  definable<T>& definable<T>::operator =(const T &i) {
    _defined = true;
    _item = i;
    return *this;
  }

  template <typename T>
  std::ostream& operator <<(std::ostream& out, definable<T>& data) {
    if (data._defined)
      out << data._item;
    else
      out << std::string("[undefined]");
    return out;
  }


}; // namespace PhotoFinish
