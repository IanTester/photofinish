/*
	Copyright 2021 Ian Tester

	This file is part of Photo Finish.

	Photo Finish is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Photo Finish is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Photo Finish.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace PhotoFinish {

  template <typename Num_type, typename R_type>
  Exiv2::ValueType<R_type>& closest_Rational(double value) {
    double margin = fabs(value) * 1e-6;
    Num_type num = 0;
    Num_type den;
    for (den = 1; den < INT_MAX; den++) {
      double numf = value * den;
      if ((numf < INT_MIN) || (numf > INT_MAX))
	break;

      num = round(numf);
      double error = fabs(num - numf);
      if (error < margin * den)
	break;
    }

    Exiv2::ValueType<R_type> *rv = new Exiv2::ValueType<R_type>(R_type(num, den));
    return *rv;
  }


}; // namespace PhotoFinish
