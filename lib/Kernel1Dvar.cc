/*
	Copyright 2014-2019 Ian Tester

	This file is part of Photo Finish.

	Photo Finish is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Photo Finish is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Photo Finish.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <iomanip>
#include <boost/algorithm/string/predicate.hpp>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <omp.h>
#include "Benchmark.hh"
#include "Kernel1Dvar.hh"

#define sqr(x) ((x) * (x))
#define min(x,y) ((x) < (y) ? (x) : (y))

namespace PhotoFinish {

  Kernel1Dvar::Kernel1Dvar() :
    _size(nullptr), _start(nullptr),
    _weights(nullptr)
  {}

  Kernel1Dvar::Kernel1Dvar(double to_size) :
    _size(nullptr), _start(nullptr),
    _weights(nullptr),
    _to_size(to_size),
    _to_size_i(ceil(to_size))
  {
    _size = new uint32_t[_to_size_i];
    _start = new uint32_t[_to_size_i];
    _weights = new SAMPLE*[_to_size_i];
  }

  void Kernel1Dvar::build(double from_start, double from_size, uint32_t from_max) {
    _scale = from_size / _to_size;
    double range, norm_fact;
    if (_scale < 1.0) {
      range = this->range();
      norm_fact = 1.0;
    } else {
      range = this->range() * _scale;
      norm_fact = this->range() / ceil(range);
    }

#pragma omp parallel for schedule(dynamic, 1)
    for (uint32_t i = 0; i < _to_size_i; i++) {
      double centre = from_start + (i * _scale);
      uint32_t left = floor(centre - range);
      if (range > centre)
	left = 0;
      uint32_t right = ceil(centre + range);
      if (right >= from_max)
	right = from_max - 1;
      _size[i] = right + 1 - left;
      _start[i] = left;
      _weights[i] = new SAMPLE[_size[i]];
      uint32_t k = 0;
      for (uint32_t j = left; j <= right; j++) {
	_weights[i][k] = this->eval((centre - j) * norm_fact);
	k++;
      }

      // normalize the filter's weight's so the sum equals to 1.0, very important for avoiding box type of artifacts
      uint32_t max = _size[i];
      SAMPLE tot = 0.0;
      for (uint32_t k = 0; k < max; k++)
	tot += _weights[i][k];
      if (fabs(tot) > 1e-5) {
	tot = 1.0 / tot;
	for (uint32_t k = 0; k < max; k++)
	  _weights[i][k] *= tot;
      }
    }
  }

  Kernel1Dvar::ptr Kernel1Dvar::create(const D_resize& dr, double from_start, double from_size, uint32_t from_max, double to_size) {
    Kernel1Dvar::ptr ret;
    if (dr.filter().length()  == 0) {
      ret = std::make_shared<Lanczos1D>(D_resize::lanczos(3.0), from_start, from_size, from_max, to_size);
      return ret;
    }

    std::string filter = dr.filter();
    if (boost::iequals(filter.substr(0, min(filter.length(), 7)), "lanczos")) {
      ret = std::make_shared<Lanczos1D>(dr, from_start, from_size, from_max, to_size);
      return ret;
    }

    throw DestinationError("resize.filter", filter);
  }

  Kernel1Dvar::~Kernel1Dvar() {
    if (_size != nullptr) {
      delete [] _size;
      _size = nullptr;
    }

    if (_start != nullptr) {
      delete [] _start;
      _start = nullptr;
    }

    if (_weights != nullptr) {
      for (uint32_t i = 0; i < _to_size_i; i++)
	delete [] _weights[i];
      delete [] _weights;
      _weights = nullptr;
    }
  }

  // Template method that does the actual horizontal convolving
  template <typename T, int channels>
  void Kernel1Dvar::convolve_h_type_channels(Image::ptr src, Image::ptr dest, bool can_free) {
#pragma omp parallel
    {
#pragma omp master
      {
	std::cerr << "Convolving image horizontally " << src->width() << " => "
		  << std::setprecision(2) << std::fixed << dest->width()
		  << " using " << omp_get_num_threads() << " threads..." << std::endl;
      }
    }

    Timer timer;
    int64_t pixel_count = 0;
    timer.start();

#pragma omp parallel for schedule(dynamic, 1)
    for (uint32_t y = 0; y < src->height(); y++) {
      auto inrow = src->row(y);
      dest->check_row_alloc(y);
      auto outrow = dest->row(y);

      if (src->format().is_planar()) {
	SAMPLE temp;
	for (uint8_t c = 0; c < channels; c++) {
	  T *outp = outrow->data<T>(0, c);
	  for (uint32_t nx = 0; nx < dest->width(); nx++) {
	    const SAMPLE *weight = _weights[nx];
	    T *inp = inrow->data<T>(_start[nx], c);
	    temp = 0;
	    for (uint32_t kx = _size[nx]; kx; kx--) {
	      temp += (*inp) * (*weight);
	      weight++;
	      inp++;
	    }
	    *outp = limitval<T>(temp);
	    outp++;
	  }
	}
      } else {
	SAMPLE temp[channels];

	for (uint32_t nx = 0; nx < dest->width(); nx++) {
	  memset(temp, 0, sizeof(temp));

	  const SAMPLE *weight = _weights[nx];
	  for (uint32_t x = _start[nx]; x < _start[nx] + _size[nx]; x++) {
	    SAMPLE *t = temp;
	    T *inp = inrow->data<T>(x);
	    for (uint8_t c = 0; c < channels; c++) {
	      *t += (*inp) * (*weight);
	      t++;
	      inp++;
	    }
	    weight++;
	  }

	  SAMPLE *t = temp;
	  T *outp = outrow->data<T>(nx);
	  for (uint8_t c = 0; c < channels; c++) {
	    *outp = limitval<T>(*t);
	    outp++;
	    t++;
	  }
	}
      }
      pixel_count += dest->width();

      if (can_free)
	src->free_row(y);

      if (omp_get_thread_num() == 0)
	std::cerr << "\r\tConvolved " << y + 1 << " of " << src->height() << " rows";
    }
    timer.stop();

    std::cerr << "\r\tConvolved " << src->height() << " of " << src->height() << " rows." << std::endl;

    if (benchmark_mode) {
      std::cerr << std::setprecision(2) << std::fixed;
      std::cerr << "Benchmark: Horizontally convolved " << pixel_count << " pixels in " << timer << " = " << (pixel_count / timer.elapsed() / 1e+6) << " Mpixels/second" << std::endl;
    }

  }

  // Template method that handles each type for horizontal convolving
  template <typename T>
  void Kernel1Dvar::convolve_h_type(Image::ptr src, Image::ptr dest, bool can_free) {
    uint8_t channels = src->format().total_channels();
    switch (channels) {
    case 1: // e.g greyscale
      convolve_h_type_channels<T, 1>(src, dest, can_free);
      break;

    case 2: // e.g greyscale with alpha
      convolve_h_type_channels<T, 2>(src, dest, can_free);
      break;

    case 3: // e.g RGB, Lab, etc
      convolve_h_type_channels<T, 3>(src, dest, can_free);
      break;

    case 4: // e.g CMYK, or RGB, Lab, etc with alpha
      convolve_h_type_channels<T, 4>(src, dest, can_free);
      break;

    case 5: // e.g CMYK with alpha
      convolve_h_type_channels<T, 5>(src, dest, can_free);
      break;

    case 6:
      convolve_h_type_channels<T, 6>(src, dest, can_free);
      break;

    case 7:
      convolve_h_type_channels<T, 7>(src, dest, can_free);
      break;

    case 8:
      convolve_h_type_channels<T, 8>(src, dest, can_free);
      break;

    case 9:
      convolve_h_type_channels<T, 9>(src, dest, can_free);
      break;

    case 10:
      convolve_h_type_channels<T, 10>(src, dest, can_free);
      break;

    case 11:
      convolve_h_type_channels<T, 11>(src, dest, can_free);
      break;

    case 12:
      convolve_h_type_channels<T, 12>(src, dest, can_free);
      break;

    case 13:
      convolve_h_type_channels<T, 13>(src, dest, can_free);
      break;

    case 14:
      convolve_h_type_channels<T, 14>(src, dest, can_free);
      break;

    case 15:
      convolve_h_type_channels<T, 15>(src, dest, can_free);
      break;

    default:
      std::cerr << "** Cannot handle " << (int)channels << " channels **" << std::endl;
    }
  }

  //! Convolve an image horizontally
  Image::ptr Kernel1Dvar::convolve_h(Image::ptr img, bool can_free) {
    auto ni = std::make_shared<Image>(_to_size_i, img->height(), img->format());
    ni->set_profile(img->profile());

    if (img->xres().defined())
      ni->set_xres(img->xres() / _scale);
    if (img->yres().defined())
      ni->set_yres(img->yres());

    switch (img->format().bytes_per_channel()) {
    case 1:
      convolve_h_type<uint8_t>(img, ni, can_free);
      break;

    case 2:
      convolve_h_type<uint16_t>(img, ni, can_free);
      break;

    case 4:
      if (img->format().is_fp())
	convolve_h_type<float>(img, ni, can_free);
      else
	convolve_h_type<uint32_t>(img, ni, can_free);
      break;

    case 8:
      convolve_h_type<double>(img, ni, can_free);
      break;

    }

    return ni;
  }

  // Template method that does the actual vertical convolving
  template <typename T, int channels>
  void Kernel1Dvar::convolve_v_type_channels(Image::ptr src, Image::ptr dest, bool can_free) {
#pragma omp parallel
    {
#pragma omp master
      {
	std::cerr << "Convolving image vertically " << src->height() << " => "
		  << std::setprecision(2) << std::fixed << dest->height()
		  << " using " << omp_get_num_threads() << " threads..." << std::endl;
      }
    }

    Timer timer;
    int64_t pixel_count = 0;
    timer.start();

#pragma omp parallel for schedule(dynamic, 1)
    for (uint32_t ny = 0; ny < dest->height(); ny++) {
      uint32_t ystart = _start[ny];
      uint32_t yend = ystart + _size[ny];

      dest->check_row_alloc(ny);
      auto outrow = dest->row(ny);

      if (src->format().is_planar()) {
	SAMPLE temp[dest->width()];
	for (uint8_t c = 0; c < channels; c++) {
	  const SAMPLE *weight = _weights[ny];
	  memset(temp, 0, sizeof(temp));

	  for (uint32_t y = ystart; y < yend; y++) {
	    T *inp = src->row(y)->data<T>(0, c);
	    SAMPLE *t = temp;
	    for (uint32_t x = 0; x < src->width(); x++) {
	      *t += (*inp) * (*weight);
	      t++;
	      inp++;
	    }
	    weight++;
	  }

	  T *outp = outrow->data<T>(0, c);
	  SAMPLE *t = temp;
	  for (uint32_t x = 0; x < src->width(); x++) {
	    *outp = limitval<T>(*t);
	    outp++;
	    t++;
	  }
	}
      } else {
	SAMPLE temp[channels];
	for (uint32_t x = 0; x < src->width(); x++) {
	  memset(temp, 0, sizeof(temp));

	  const SAMPLE *weight = _weights[ny];
	  for (uint32_t y = ystart; y < yend; y++) {
	    SAMPLE *t = temp;
	    auto inrow = src->row(y);
	    T *inp = inrow->data<T>(x);
	    for (uint8_t c = 0; c < channels; c++) {
	      *t += (*inp) * (*weight);
	      t++;
	      inp++;
	    }
	    weight++;
	  }

	  T *outp = outrow->data<T>(x);
	  SAMPLE *t = temp;
	  for (uint8_t c = 0; c < channels; c++) {
	    *outp = limitval<T>(*t);
	    outp++;
	    t++;
	  }
	}
      }
      pixel_count += dest->width();

      if (can_free)
	src->free_row(ny);

      if (omp_get_thread_num() == 0)
	std::cerr << "\r\tConvolved " << ny + 1 << " of " << _to_size_i << " rows";
    }
    timer.stop();

    std::cerr << "\r\tConvolved " << _to_size_i << " of " << _to_size_i << " rows." << std::endl;

    if (benchmark_mode) {
      std::cerr << std::setprecision(2) << std::fixed;
      std::cerr << "Benchmark: Vertically convolved " << pixel_count << " pixels in " << timer << " = " << (pixel_count / timer.elapsed() / 1e+6) << " Mpixels/second" << std::endl;
    }

  }

  // Template method that handles each type for vertical convolving
  template <typename T>
  void Kernel1Dvar::convolve_v_type(Image::ptr src, Image::ptr dest, bool can_free) {
    uint8_t channels = src->format().total_channels();
    switch (channels) {
    case 1:
      convolve_v_type_channels<T, 1>(src, dest, can_free);
      break;

    case 2:
      convolve_v_type_channels<T, 2>(src, dest, can_free);
      break;

    case 3:
      convolve_v_type_channels<T, 3>(src, dest, can_free);
      break;

    case 4:
      convolve_v_type_channels<T, 4>(src, dest, can_free);
      break;

    case 5:
      convolve_v_type_channels<T, 5>(src, dest, can_free);
      break;

    case 6:
      convolve_v_type_channels<T, 6>(src, dest, can_free);
      break;

    case 7:
      convolve_v_type_channels<T, 7>(src, dest, can_free);
      break;

    case 8:
      convolve_v_type_channels<T, 8>(src, dest, can_free);
      break;

    case 9:
      convolve_v_type_channels<T, 9>(src, dest, can_free);
      break;

    case 10:
      convolve_v_type_channels<T, 10>(src, dest, can_free);
      break;

    case 11:
      convolve_v_type_channels<T, 11>(src, dest, can_free);
      break;

    case 12:
      convolve_v_type_channels<T, 12>(src, dest, can_free);
      break;

    case 13:
      convolve_v_type_channels<T, 13>(src, dest, can_free);
      break;

    case 14:
      convolve_v_type_channels<T, 14>(src, dest, can_free);
      break;

    case 15:
      convolve_v_type_channels<T, 15>(src, dest, can_free);
      break;

    default:
      std::cerr << "** Cannot handle " << (int)channels << " channels **" << std::endl;
    }
  }

  //! Convolve an image vertically
  Image::ptr Kernel1Dvar::convolve_v(Image::ptr img, bool can_free) {
    auto ni = std::make_shared<Image>(img->width(), _to_size_i, img->format());
    ni->set_profile(img->profile());

    if (img->xres().defined())
      ni->set_xres(img->xres());
    if (img->yres().defined())
      ni->set_yres(img->yres() / _scale);

    switch (img->format().bytes_per_channel()) {
    case 1:
      convolve_v_type<uint8_t>(img, ni, can_free);
      break;

    case 2:
      convolve_v_type<uint16_t>(img, ni, can_free);
      break;

    case 4:
      if (img->format().is_fp())
	convolve_v_type<float>(img, ni, can_free);
      else
	convolve_v_type<uint32_t>(img, ni, can_free);
      break;

    case 8:
      convolve_v_type<double>(img, ni, can_free);
      break;

    }

    return ni;
  }



  Lanczos1D::Lanczos1D() :
    Kernel1Dvar()
  {}

    Lanczos1D::Lanczos1D(const D_resize& dr, double from_start, double from_size, uint32_t from_max, double to_size) :
      Kernel1Dvar(to_size),
      _radius(dr.support()),
      _r_radius(_radius.defined() ? 1.0 / _radius : 0.0)
  {
    build(from_start, from_size, from_max);
  }

  double Lanczos1D::range(void) const {
    return _radius;
  }

  SAMPLE Lanczos1D::eval(double x) const {
    if (!_radius.defined())
      throw Uninitialised("Lanczos1D", "resize.radius");

    if (fabs(x) < 1e-6)
      return 1.0;
    double pix = M_PI * x;
    return (_radius * sin(pix) * sin(pix * _r_radius)) / (sqr(M_PI) * sqr(x));
  }

}
